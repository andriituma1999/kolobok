package cz.vse.java.tuma02.adventurasem.logika;

 

/*******************************************************************************
 * Třída Vec ...
 *
 *@author     Andrii Tuma
 *@version   1.0.0. Autumn 2019
 */
public class Vec
{
//== Datové atributy (statické i instancí)======================================
    private String jmeno;
    private boolean prenositelna;

//##############################################################################
//== Konstruktory a tovární metody =============================================

    /***************************************************************************
     *
     */
    public Vec (String jmeno, boolean prenositelna) {
		this.jmeno = jmeno;
		this.prenositelna = prenositelna;
	}



//== Nesoukromé metody (instancí i třídy) ===============================================
//== Soukromé metody (instancí i třídy) ===========================================
    public String getJmeno () {
		return jmeno;
	}
	public boolean jePrenositelna() {
		return prenositelna;
	}

}

