package cz.vse.java.tuma02.adventurasem.main;
/*
 *@author     Andrii Tuma
 *@version    1.0.0. Autumn 2019
 */
public interface Subject {

    /**
     * Registrace pozorovatele změn
     * @param observer
     */
    void registerObserver(Observer observer);

    /**
     * Odebrání ze seznamu pozorovatelů
     * @param observer
     */
    void unregisterObserver(Observer observer);

    /**
     * Upozornění registrovaných pozorovatelů na změnu
     */
    void notifyObservers();
}
