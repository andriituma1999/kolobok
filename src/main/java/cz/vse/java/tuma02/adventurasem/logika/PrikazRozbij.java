package cz.vse.java.tuma02.adventurasem.logika;

/**
 *  Třída PrikazRozbij implementuje pro hru příkaz Rozbij.
 *  Tato třída je součástí moje textové hry.
 *  Rozbije  zvolený předmět Sekerou,  když sekera je v batohu.Syntaxe je 'rozbij Objekt '
 *@author     Andrii Tuma
 *@version    1.0.0. Autumn 2019
 */
public class PrikazRozbij implements IPrikaz{


        public static final String NAZEV = "rozbij";
        private HerniPlan plan;
        private Batoh batoh;
        /**
         *  Konstruktor třídy
         *
         *  @param plan herní plán, ve kterém se bude ve hře "chodit", batoh, který použivá
         */
        public PrikazRozbij(HerniPlan plan, Batoh batoh) {
            this.plan = plan;
            this.batoh = batoh;
        }

        /**
         *  Provádí příkaz "rozbij". Zkouší rozbit zadanou cil. Pokud cil je Prekazlivy_ker a
         *  má při sobě předmět Sekera, tak nebude existovat Prekazlivy_ker v aktualním prostoru,
         *  otevře se nový východ.
         *  Pokud podminky nejsou splněný ( Prekazlivy_ker,  Sekera),
         *    vypíše se chybové hlášení.
         *
         *@param parametry - jako  parametr obsahuje cil prostoru (východu),
         *                         který má rozbit.
         *@return zpráva, kterou vypíše hra hráči
         */
        @Override
        public String proved(String... parametry) {
            if (parametry.length == 0) {
                // pokud chybí druhé slovo (cil rozbiti), tak ....
                return "Co mám rozbit? Musíš zadat jméno cile";
            }

            String cil = parametry[0];

            // zkoušíme rozbit cil
            Vec vec = batoh.vyberVec("Sekera");
            if (vec == null){
                return  "Nemám čim to rozbit";
            }
            if (cil.equals(Texts.PŘEKAŽLIVÝ_KEŘ) && plan.getAktualniProstor().rozbitiVeci(cil) == true
            && vec != null) {
                plan.getAktualniProstor().getSeznamVeci().remove(cil);
                plan.getAktualniProstor().setVychod(plan.getSilnicka());
                batoh.vlozVec(vec);
                plan.notifyObservers();
                return "Cesta dal je otevřená. Pojdívame se co je to za silnička";
            } else {
                return "Nemužu rozbit tuto cil";
            }
        }
        /**
         *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
         *
         *  @ return nazev prikazu
         */
        @Override
        public String getNazev(){
            return NAZEV;
        }

    }




