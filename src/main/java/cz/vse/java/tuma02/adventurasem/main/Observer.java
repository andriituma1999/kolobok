package cz.vse.java.tuma02.adventurasem.main;

/*
 *@author     Andrii Tuma
 *@version    1.0.0. Autumn 2019
 */

public interface Observer {

    /**
     * reakce na pozorovanou změnu
     */
    void update();
}
