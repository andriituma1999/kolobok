package cz.vse.java.tuma02.adventurasem.logika;
/**
 *  Třída PrikazSpat implementuje pro hru příkaz spat.
 *  Tato třída je součástí moje textové hry.
 *
 *@author     Andrii Tuma
 *@version    1.0.0. Autumn 2019

 *
 */
public class PrikazSpat implements IPrikaz{


        private static final String NAZEV = "spat";


        /**
         *prostě Kolobok se vyspí. Neovlivní průběh hry
         *  @return sprava ze spání ke hre
         */
        @Override
        public String proved(String... parametry) {
            return Texts.zVYSPANO;

        }

        /**
         *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
         *
         *  @ return nazev prikazu
         */
        @Override
        public String getNazev() {
            return NAZEV;
        }

    }

