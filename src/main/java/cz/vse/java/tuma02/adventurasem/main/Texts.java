/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.vse.java.tuma02.adventurasem.main;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 *
 * @author Andrii TUMA
 * @version 1.0.0.  2019 Summer
 */
public class Texts {
//== CONSTANT CLASS ATTRIBUTES =================================================

    /**
     * Jméno autora programu.
     */
    static final String AUTHOR_NAME = "TUMA Andrii";

    /**
     * Xname autora programu.
     */
    static final String AUTHOR_ID = "TUMA02";

    /**
     * Názvy používaných prostorů - místností.
     */
    static final String HAJ = "Haj",
            DŮM = "Dum",
            LES = "Les",
            HAJ_VLKA = "Haj_vlka",
            TEMNÝ_LES = "Temny_les",
            SILNIČKA = "Silnička";

    /**
     * Názvy používaných h-objektů.
     */
    static final String JAHODY = "Jahody",
            JAHODY_LC = "jahody", //LC
            KLECE = "Klece", 
            KLECE_LC = "klece", //LC
            KEŘ = "Ker",
            KEŘ_LC = "ker",
            PILKA = "Pilka",
            
            PILKA_LC = "pilka", //LC    
            PŘEKAŽLIVÝ_KEŘ = "Prekazlivy_ker",
            PŘEKAŽLIVÝ_KEŘ_LC = "prekazlivy_ker", //LC
            SEKERA = "Sekera",
            SEKERA_LC = "sekera";


    /**
     * Názvy používaných akcí.
     */
    static final String pHELP = "?",
            pVEZMI = "Vezmi",
            pJDI = "Jdi",
            pPOLOŽ = "Poloz";

    /**
     * Názvy používaných NEOBYČEJNÝCH akcí.
     */
    static final String pPROZKOUMEJ = "Prozkoumej",
            pROZBIJ = "Rozbij",
            pROZPIL = "Rozpil",
            pKONEC = "Konec",
            pPODÍVEJ = "Podivej",
            pVSTUP = "Vstup",
            pSPAT = "Spat";
    /**
     * Formát dodatku zprávy informujícího o aktuálním stavu hráče.
     */
    static final String SOUSEDÉ = "Sousedé: ",
            H_OBJEKTY = "H-objekty: ",
            BATOH = "Batoh: ",
            FORMÁT_INFORMACE = "\n\nNacházíte se v: %s"
            + "\n" + SOUSEDÉ + "[%s]"
            + "\n" + H_OBJEKTY + "[%s]"
            + "\n" + BATOH + "[%s]";

    /**
     * Texty zpráv vypisovaných v reakci na příkazy vyvolávají povinné akce.
     * Počáteční z (zpráva) slouží k odlišení od stavů.
     */
    static final String zPORADÍM = "\nChcete-li poradit, zadejte příkaz ?",
            /**
             * Reakce na chyby
             */
            zNENÍ_START = "\nPrvním příkazem není startovací příkaz."
            + "\nHru, která neběží, lze spustit pouze startovacím příkazem.\n",
            zPRÁZDNÝ_PŘÍKAZ = "\nZadal(a) jste prázdný příkaz." + zPORADÍM,
            zNEZNÁMÝ_PŘÍKAZ = "\nTento příkaz neznám." + zPORADÍM,
            zANP = "\nZadaná akce nebyla provedena",
          zCIL_NEZADAN = zANP + "\nNebyla zadána místnost, do níž se má přejít",
            zNENÍ_CIL = zANP + "\nDo zadané místnosti se odsud nedá přejít: ",
    zH_OBJEKT_NEZADAN = zANP + "\nNebyl zadán h-objekt, s nímž mám manipulovat",
            zTĚŽKÝ_H_OBJEKT = zANP + "\nZadaný h-objekt nejde zvednout: ",
            zNENÍ_H_OBJEKT = zANP + "\nZadaný h-objekt zde není: ",
            zNENÍ_V_BATOHU = zANP + "\nTento objekt nedržíte: ",
            zBATOH_PLNÝ = zANP
            + "\nZadaný h-objekt nemůžete vzít, máte už obě ruce plné",
            zNEJDE_ROZBIT = "Daný předmět rozbit nemužete.",
            zNEJDE_ROZPILIT="Daný předmět rozpilit nemužete.",
            zNÁPOVĚDA = "Vítejte!\n" +
            "Toto je příběh o Kolobkovi a lišce. Třeba ho přivest domu.  \n" 
                
            + "\nPříkazy, které je možno v průběhu hry zadat:"
            + "\n============================================\n",
            
           
           
            /**
             * Reakce na úspěšné provedení příkazu
             */
            zPŘESUN = "\nVešli jste do: ",
            zZVEDNUTO = "\nDali jste si do batohu : ", 
            zPROZKOUMÁNO = "\nProzkoumali jste: ", 
            zROZBITO = "\nRozbili jste: ",
            zROZPILENO = "\nRozpilili jste: ",
            zPOLOŽENO = "\nPoložili jste ", 
            zVYSPANO = "\n Kolobok lehl spat a probudil se za nějaký čas",
         
          
            zGRATULACE = "\nGratuluji, dohráli jste mou hru Kolobok. "
            
            + "\nPřeji krásný den, Andrii Tuma - tuma02"
            + "\nHru ukončíte příkazem: " + pKONEC + "'",
            /**
             * Vítání
             */
            zUVÍTÁNÍ
            = "Vítejte!\n"
            + "Toto je příběh o Kolobkovi a lišce. Třeba ho přivest domu.  \n"
            + "Nevíte-li, jak pokračovat, zadejte příkaz 'napoveda'.\n",
            zCELÉ_UVÍTÁNÍ = zUVÍTÁNÍ
            + String.format(FORMÁT_INFORMACE,
                    LES, cm(HAJ, TEMNÝ_LES),
                    cm(), cm()),
            /**
             * Speciální informační bloky typické pro určité situace, nebo
             * místa.
             */
           zVLK = "Šli jste tam,  kam není třeba a vaše hra je ukončena. \n"
            + "Vas snědl vlk. Pro ukončení zadejte přikaz \"Konec\".",
          zINFO_DRAHA = "\nDraha je uklizená. Je možna pokračovat dál.", //moje
            zINFO_NA_SILNIČCE = "\nKolobok našel silnice, šel po ní a uviděl "
            + "lišku, ktera\n"
                        + "se chytila do pasti - dřevene klece ",
           zINFO_NA_SILNIČCE2 = "Liška uspěšně vylezla z pasti",
            zINFO_DUM = "\nKolobok pomohl lišce a ta ho přivedla k jeho domu ",
           
            zKONEC = "\nKonec hry. \nDěkujeme, že jste zkusil(a) naši hru.";

//== VARIABLE CLASS ATTRIBUTES =================================================
//##############################################################################
//== STATIC INITIALIZER (CLASS CONSTRUCTOR) ====================================
//== CLASS GETTERS AND SETTERS =================================================
//== OTHER NON-PRIVATE CLASS METHODS ===========================================
    /**
     * *************************************************************************
     * Vrátí řetězec obsahující zadané názvy oddělené čárkami.
     *
     * @param názvy Názvy, které je třeba sloučit
     * @return Výsledný řetězec ze sloučených zadaných názvů
     */
    static String cm(String... názvy) {
        String result = Arrays.stream(názvy)
                .collect(Collectors.joining(", "));
        return result;
    }
  
//##############################################################################
//== CONSTUCTORS AND FACTORY METHODS ===========================================
    /**
     * Soukromý konstruktor zabraňující vytvoření instance.
     */
    private Texts() {
    }
}
