package cz.vse.java.tuma02.adventurasem.logika;
/**
 *  Třída PrikazPodivejse implementuje pro hru příkaz podivejse Object.
 *@author     Andrii Tuma
 *@version    1.0.0. Autumn 2019
 */
public class PrikazPodivejse implements IPrikaz {


        private static final String NAZEV = "podivejse";
        private HerniPlan plan;

        /**
         *  Konstruktor třídy
         *
         *  @param plan herní plán, ve kterém se bude hledat aktuální místnost
         */
        public PrikazPodivejse(HerniPlan plan) {
            this.plan = plan;

        }
        /*použiva se pro prozkoumaní věci. Když hrač prozkouma vlka, hra se ukonči*/
        public String proved(String... parametry) {
            if (parametry.length == 0) {
                // pokud chybí druhé slovo , tak ....
                return "Na co se mám podivat? Musíš zadat jméno věci";
            }

            String jmenoVeci = parametry[0];


            if (jmenoVeci.equals("Vlk") && plan.getAktualniProstor().rozbitiVeci(jmenoVeci) == true) {
                plan.getAktualniProstor().removeVychod();
                plan.notifyObservers();
                return Texts.zVLK  ;
            }
            else {
               if( plan.getAktualniProstor().rozbitiVeci(jmenoVeci) == true){
                   return "prostě " + jmenoVeci + ", níc neobyčejného";
               }
               else return "takové věci tady není";
            }
        }
    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     *  @ return nazev prikazu
     */
        public String getNazev() {
            return NAZEV;
        }
    }



