package cz.vse.java.tuma02.adventurasem.main;

import cz.vse.java.tuma02.adventurasem.logika.Hra;
import cz.vse.java.tuma02.adventurasem.logika.IHra;
import cz.vse.java.tuma02.adventurasem.logika.PrikazJdi;
import cz.vse.java.tuma02.adventurasem.logika.Prostor;
import cz.vse.java.tuma02.adventurasem.logika.*;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
/*
 * Controller ve kterem je napsano všechno, pro ovladaní tlačitek v GUI
*@author     Andrii Tuma
*@version    1.0.0. Autumn 2019
 */


public class HomeController extends GridPane implements Observer {
    @FXML
    public TextField vstup;
    @FXML
    public TextArea vystup;
    @FXML
    public Button odesli;
    @FXML
    public ListView seznamVychodu;
    @FXML
    public ImageView hrac;
    @FXML
    private ListView inventar;
    @FXML
    public MenuItem newGame;
    @FXML
    public MenuItem napoveda;

    private Map<String, Point2D> souradniceProstoru = new HashMap<>();

    private IHra hra = new Hra();
    /*initializuje hru při spouštění*/
    public void initialize() {
        vystup.setText(hra.vratUvitani()+"\n\n");
        vystup.setEditable(false);

        hra.getHerniPlan().registerObserver(this);
         hra.getBatoh().registerObserver(this);
        souradniceProstoru.put(Texts.LES,new Point2D(65,25));
        souradniceProstoru.put(Texts.HAJ,new Point2D(170,25));
        souradniceProstoru.put(Texts.TEMNÝ_LES,new Point2D(15,104));
        souradniceProstoru.put(Texts.HAJ_VLKA,new Point2D(275,75));
        souradniceProstoru.put(Texts.SILNIČKA,new Point2D(65,183));
        souradniceProstoru.put(Texts.DŮM,new Point2D(170,183));

        update();
    }

     /*Metoda začne hru znova*/
    public void toStartNewGame(ActionEvent actionEvent){
        vstup.setDisable(false);
        odesli.setDisable(false);
        seznamVychodu.setDisable(false);
        hra = new Hra();
        initialize();
    }
    /*Metoda vytvařuje nove okno s napovědou pomoci klasu NapovedaStart*/
    public void getNapoveda(ActionEvent actionEvent){
     NapovedaStart napovedaStart = new NapovedaStart();
     try {
         napovedaStart.start(new Stage());
     } catch(Exception e){
         e.printStackTrace();
     }
    }
    /*převede vvod na pole vstupu v GUI*/
    public void zaktivniVstup() {
        vstup.requestFocus();
    }

    /*spracuje přikaz, který byl zpusoben tlačitkama*/
    public void zpracujVstup(ActionEvent actionEvent) {
        zpracujPrikaz(vstup.getText());
    }

    /*spracuje přikaz, který byl napsan v přikazové řadce*/
    private void zpracujPrikaz(String prikaz) {
        vystup.appendText("Příkaz: "+prikaz+"\n");
        String vysledek = hra.zpracujPrikaz(prikaz);
        vystup.appendText(vysledek+"\n\n");
        vstup.clear();

        if(hra.konecHry()) {
            vystup.appendText(hra.vratEpilog());
            vstup.setDisable(true);
            odesli.setDisable(true);
            seznamVychodu.setDisable(true);
        }
    }

    @Override
    /**
     * reakce na pozorovanou změnu
     */
    public void update() {
        System.out.println("aktualizace");
        seznamVychodu.getItems().clear();
        seznamVychodu.getItems().addAll(hra.getHerniPlan().getAktualniProstor().getVychody());

        String nazevProstoru = hra.getHerniPlan().getAktualniProstor().getNazev();
        hrac.setX(souradniceProstoru.get(nazevProstoru).getX());
        hrac.setY(souradniceProstoru.get(nazevProstoru).getY());

        inventar.getItems().clear();
        Set<String> seznam = hra.getBatoh().vratVeci();
        for(String vec : seznam){
            ImageView obrazek = new ImageView();
            obrazek.setImage(new Image("/inventar/"+vec.toLowerCase()+".png"));
            inventar.getItems().add(obrazek);
        }
        zaktivniVstup();
    }
    /*algorytm při kliknutí na vychod v GUI*/
    public void kliknutiNaVychod(MouseEvent mouseEvent) {
        Prostor prostor = (Prostor) seznamVychodu.getSelectionModel().getSelectedItem();
        zpracujPrikaz(PrikazJdi.NAZEV+Hra.ODDELOVAC+prostor.getNazev());
    }
}
