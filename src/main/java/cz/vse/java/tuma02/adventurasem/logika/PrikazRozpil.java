package cz.vse.java.tuma02.adventurasem.logika;
/**
 *  Třída PrikazRozpil implementuje pro hru příkaz Rozpil.
 *  Tato třída je součástí moje textové hry.
 *  Rozpilí  zvolený předmět pilou,  když pilka je v batohu.Syntaxe je 'Rozpil Objekt '
 *@author     Andrii Tuma
 *@version    1.0.0. Autumn 2019
 */
public class PrikazRozpil implements IPrikaz{

        public static final String NAZEV = "rozpil";
        private HerniPlan plan;
        private Batoh batoh;
        /**
         *  Konstruktor třídy
         *
         *  @param plan herní plán, ve kterém se bude ve hře "chodit", batoh, který použivá
         */
        public PrikazRozpil(HerniPlan plan, Batoh batoh) {
            this.plan = plan;
            this.batoh = batoh;
        }

        /**
         *  Provádí příkaz "rozpil". Zkouší rozpilit zadanou cil. Pokud cil je Klece a
         *  má při sobě předmět Pilka, tak nebude existovat Klece v aktualním prostoru,
         *  otevře se nový východ.
         *  Pokud podminky nejsou splněný ( Klece,  Pilka),
         *    vypíše se chybové hlášení.
         *
         *@param parametry - jako  parametr obsahuje cil prostoru (východu),
         *                         který má rozbit.
         *@return zpráva, kterou vypíše hra hráči
         */
        @Override
        public String proved(String... parametry) {
            if (parametry.length == 0) {
                // pokud chybí druhé slovo (cil rozpilení), tak ....
                return "Co mám rozpilit? Musíš zadat jméno cile";
            }

            String cil = parametry[0];

            // zkoušíme rozpilit cil
            Vec vec = batoh.vyberVec("Pilka");
            if (vec == null){
                return  "Nemám čim to rozpilit";
            }
            if (cil.equals(Texts.KLECE) && plan.getAktualniProstor().rozbitiVeci(cil) == true
                    && vec != null) {
                plan.getAktualniProstor().getSeznamVeci().remove(cil);
                plan.getAktualniProstor().setVychod(plan.getDum());
                batoh.vlozVec(vec);
                plan.notifyObservers();
                return Texts.zINFO_NA_SILNIČCE2;
            } else {
                return "Nemužu rozpilit tuto cil";
            }
        }
        /**
         *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
         *
         *  @ return nazev prikazu
         */
        @Override
        public String getNazev(){
            return NAZEV;
        }
}
