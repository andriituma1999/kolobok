package cz.vse.java.tuma02.adventurasem.logika;

 

import cz.vse.java.tuma02.adventurasem.main.Observer;
import cz.vse.java.tuma02.adventurasem.main.Subject;

import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;

/**
 *  Trida Batoh 
 *
 *
 *@author     Andrii Tuma
 *@version    1.0.0. Autumn 2019
 */

public class Batoh implements Subject
{

public static final int KAPACITA = 3 ;
private Map<String, Vec> seznamVeci ;   // seznam věcí v batohu
    private Set<Observer> seznamPozorovatelu = new HashSet<>();
public Batoh () {
seznamVeci = new HashMap<String, Vec>();
}
 /**
     * Vloží věc do batohu
     *
     *@param  vec  instance věci, která se má vložit
     */
   public void vlozVec (Vec vec) {
     seznamVeci.put(vec.getJmeno(),vec);
    }
     /**
     * Vrací řetězec názvů věcí, které jsou v batohu

     *@return            řetězec názvů
     */
    public String nazvyVeci () {
        String nazvy = "věci v batohu: ";
        for (String jmenoVeci : seznamVeci.keySet()){
            	nazvy += jmenoVeci + " ";
        }
        return nazvy;
    }
     /**
     * Hledá věc daného jména a pokud je v batohu, tak ji vrátí a vymaže ze seznamu

     *@param  jmeno   Jméno věci
     *@return            věc nebo
     *                   hodnota null, pokud tam věc daného jména není 
     */
    public Vec vyberVec (String jmeno) {
        Vec nalezenaVec = null;
        if (seznamVeci.containsKey(jmeno)) {
            nalezenaVec = seznamVeci.get(jmeno);
            seznamVeci.remove(jmeno);
        }   
        return nalezenaVec;
    }

    public Set<String> vratVeci(){
        return this.seznamVeci.keySet();
    }

    @Override
    /**
     * Registrace pozorovatele změn
     * @param observer
     */

    public void registerObserver(Observer observer) {
        seznamPozorovatelu.add(observer);
    }

    @Override
    /**
     * Odebrání ze seznamu pozorovatelů
     * @param observer
     */
    public void unregisterObserver(Observer observer) {
        seznamPozorovatelu.remove(observer);
    }

    @Override
    /**
     * Upozornění registrovaných pozorovatelů na změnu
     */
    public void notifyObservers() {
        for(Observer observer : seznamPozorovatelu) {
            observer.update();
        }
    }
}



