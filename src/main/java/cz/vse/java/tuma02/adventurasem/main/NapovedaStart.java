package cz.vse.java.tuma02.adventurasem.main;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
/*
 * Klas pro vytvaření nového Stage a zejmena napovědy, která se zobrazuje v z html failu
 *@author     Andrii Tuma
 *@version    1.0.0. Autumn 2019
 */

public class NapovedaStart {
    /*metoda, prostřednictvim ní se vytvořuje Stage pro napovědu*/
    public void start(Stage primaryStage) throws Exception {
        WebView webView = new WebView();

        webView.getEngine().load(getClass().getResource("/napoveda.html").toExternalForm());

        primaryStage.setScene(new Scene(webView, 1200, 650));

        primaryStage.show();
    }
}
