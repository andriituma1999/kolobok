package cz.vse.java.tuma02.adventurasem.logika;


import cz.vse.java.tuma02.adventurasem.main.Observer;
import cz.vse.java.tuma02.adventurasem.main.Subject;

import java.util.HashSet;
import java.util.Set;

/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 * 
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů 
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *@author     Andrii Tuma
 *@version    1.0.0. Autumn 2019
 */
public class HerniPlan implements Subject {
    
    private Prostor aktualniProstor;
    private Prostor viteznyProstor;
    private  Prostor silnicka;
    private  Prostor dum;
    private Set<Observer> seznamPozorovatelu = new HashSet<>();
    
     /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan() {
        zalozProstoryHry();

    }
    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví domeček.
     */
    private void zalozProstoryHry() {
        // vytvářejí se jednotlivé prostory
         dum = new Prostor(Texts.DŮM,Texts.zINFO_DUM);
        Prostor haj = new Prostor(Texts.HAJ, "hezký hajek za lesem");
        Prostor haj_vlka = new Prostor(Texts.HAJ_VLKA,"haj, ve kterém bydlí vlk");
        Prostor les = new Prostor(Texts.LES,"les  ve kterém se stratil naš hrdina");
        Prostor temny_les = new Prostor(Texts.TEMNÝ_LES,"temný les, z kterého těžko najít vychod");
         silnicka = new Prostor(Texts.SILNIČKA,Texts.zINFO_NA_SILNIČCE);
        // přiřazují se průchody mezi prostory (sousedící prostory)
        haj_vlka.setVychod(haj);
        haj.setVychod(haj_vlka);
        haj.setVychod(les);
        les.setVychod(haj);
        les.setVychod(temny_les);
        temny_les.setVychod(les);
        //temny_les.setVychod(silnicka);
        silnicka.setVychod(temny_les);
       // silnicka.setVychod(dum);
        dum.setVychod(silnicka);

        aktualniProstor = les;  // hra začíná v domečku
        viteznyProstor = dum ;
        haj.vlozVec(new Vec(Texts.SEKERA, true));
        haj.vlozVec(new Vec(Texts.PILKA, true));
        haj.vlozVec(new Vec(Texts.KEŘ, false));
        haj_vlka.vlozVec(new Vec("Vlk", false));
        temny_les.vlozVec(new Vec(Texts.JAHODY, true));
        temny_les.vlozVec(new Vec(Texts.PŘEKAŽLIVÝ_KEŘ, false));
        silnicka.vlozVec(new Vec(Texts.KLECE, false));
    }
    
    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */
    
    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }
    
    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {
       aktualniProstor = prostor;
        notifyObservers();
    }
    /**
     *  Metoda vrací odkaz na vítězný prostor.
     *
     *@return     vítězný prostor
     */
    
    public Prostor getViteznyProstor() {
        return viteznyProstor;
    }
    /**
     * Registrace pozorovatele změn
     * @param observer
     */
    @Override
    public void registerObserver(Observer observer) {
        seznamPozorovatelu.add(observer);
    }

    /**
     * Odebrání ze seznamu pozorovatelů
     * @param observer
     */
    @Override
    public void unregisterObserver(Observer observer) {
        seznamPozorovatelu.remove(observer);
    }
    /**
     * Upozornění registrovaných pozorovatelů na změnu
     */
    @Override
    public void notifyObservers() {
        for(Observer observer : seznamPozorovatelu) {
            observer.update();
        }
    }
    /*gettery pro održení specialních prostoru*/
    public Prostor getSilnicka(){
        return silnicka;
    }
    public Prostor getDum(){
        return dum;
    }
}
