package cz.vse.java.tuma02.adventurasem.logika;

/**
 *  Třída PrikazObsahBatohu implementuje pro hru příkaz obsahBatohu.
 *@author     Andrii Tuma
 *@version    1.0.0. Autumn 2019
 */


public class PrikazObsahBatohu implements IPrikaz
{
private static final String NAZEV = "obsahBatohu";
    private Batoh batoh;
  /**
    *  Konstruktor třídy
    *  
    *  @param plan herní plán, ve kterém se bude hledat aktuální místnost 
    */      
    public PrikazObsahBatohu( Batoh batoh) {
        this.batoh = batoh;
    }
    /**
     *  Provádí příkaz "obsahBatohu". Vypíše názvy věcí v batohu
     *  
     *@return zpráva, kterou vypíše hra hráči
     */ 
    public String proved(String... parametry) {
            return batoh.nazvyVeci();
    }
    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     *  @ return nazev prikazu
     */
    public String getNazev() {
        return NAZEV;
    }

}


